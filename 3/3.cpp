#include <omp.h>
#include <iostream>
#include <vector>

using Vector2D = std::vector<std::vector<int>>;

auto calc(const Vector2D &arr)
{
  int res = 0;

  #pragma omp parallel for reduction(+ : res)
  for(int i = 2; i < arr.size(); ++i)
  {
    for(int j = 2; j < arr[i].size(); ++j)
    {
      auto iNum = arr[i][j];
      res += (3 * iNum - 2) / ( 3 * (iNum * iNum) );
    }
  }

  return res;
}

int main()
{
  Vector2D arr = {
                  {1,3}
                 };

  auto res = calc(arr);
  std::cout << res << std::endl;

  return 0;
}
