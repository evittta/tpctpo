// Compile and run on Linux:
// g++ -std=c++14 -fopenmp rozr.cpp && ./a.out

#include <iostream>
#include <iomanip>
#include <chrono>

// Алгоритм сортування вставками (ітераційна)
std::uint64_t insertionSort(int * arr, const int& size)
{
  auto start = std::chrono::high_resolution_clock::now();

  int key, j;
  for (int i = 1; i < size; ++i)
  {
    key = arr[i];
    j = i-1;

    while (j >= 0 && arr[j] > key)
    {
      arr[j+1] = arr[j];
      j = j-1;
    }

    arr[j+1] = key;
  }

  auto finish  = std::chrono::high_resolution_clock::now();

  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( finish - start ).count();
  return duration;
}

// Алгоритм сортування вставками (паралельна)
std::uint64_t parallelInsertionSort(int * arr, const int& size)
{
  auto start = std::chrono::high_resolution_clock::now();

  int key, j;
  #pragma omp parallel for shared(arr) private(key, j)
  for (int i = 1; i < size; ++i)
  {
    key = arr[i];
    j = i-1;

    while (j >= 0 && arr[j] > key)
    {
      arr[j+1] = arr[j];
      j = j-1;
    }

    arr[j+1] = key;
  }

  auto finish  = std::chrono::high_resolution_clock::now();

  auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( finish - start ).count();
  return duration;
}


// Алгоритм сортування вибором (паралельна версія)
void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}

std::uint64_t parallelBubbleSort(int arr[], int n)
{
   auto start = std::chrono::high_resolution_clock::now();

   #pragma omp parallel for shared(arr)
   for (int i = 0; i < n-1; i++)
       for (int j = 0; j < n-i-1; j++)
           if (arr[j] > arr[j+1])
              swap(&arr[j], &arr[j+1]);


   auto finish  = std::chrono::high_resolution_clock::now();
   auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( finish - start ).count();
   return duration;
}

// helpers
void fillArray(int * arr1, int * arr2, int * arr3, const int& size)
{
  #pragma omp parallel for shared(arr1)
  for(int i = 0; i < size; ++i)
  {
    arr1[i] = rand() % 100;
    arr2[i] = arr1[i];
    arr3[i] = arr1[i];
  }
}

int main()
{
  srand((unsigned)time(0));

  int sizes[] = { 1000, 5000, 10000, 50000, 100000, 500000, 1000000 };

  for (auto size : sizes)
  {
    // generate three arrays, one for every sort method
    int * arr_1 = new int[size];
    int * arr_2 = new int[size];
    int * arr_3 = new int[size];

    fillArray(arr_1, arr_2, arr_3, size);

    // do the sort
    auto timeInsertion         = insertionSort(arr_1, size);
    auto timeInsertionParallel = parallelInsertionSort(arr_2, size);
    auto timeSelectionParellel = parallelBubbleSort(arr_3, size);

    // print results
    std::cout << "Array size : " << std::setw(9) << size << " inser seq : " << std::setw(9) << timeInsertion           << " milliseconds" << std::endl;
    std::cout << "Array size : " << std::setw(9) << size << " inser par : " << std::setw(9) << timeInsertionParallel   << " milliseconds" << std::endl;
    std::cout << "Array size : " << std::setw(9) << size << " bubbl par : " << std::setw(9) << timeSelectionParellel   << " milliseconds" << std::endl;

    // free mem
    delete[] arr_1;
    delete[] arr_2;
    delete[] arr_3;
  }

  return 0;
}
