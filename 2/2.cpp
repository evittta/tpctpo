#include <omp.h>
#include <iostream>
#include <vector>

auto parallelSum(const std::vector<int> &v1, const std::vector<int> &v2)
{
  if(v1.size() != v2.size())
    throw std::runtime_error("Vectors must be the same size.");

  std::vector<int> v3;
  v3.resize(v1.size());

  #pragma omp parallel for
  for (int i = 0; i < v1.size(); i++)
    v3[i] = v1[i] + v2[i];

  return v3;
}

int main()
{
  std::vector<int> v1 = {1,1,1,1};
  std::vector<int> v2 = {2,2,2,2};

  std::vector<int> v3 = parallelSum(v1, v2);

  for(auto i : v3)
    std::cout << i << ' ';

  return 0;
}
