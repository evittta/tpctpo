#include <omp.h>
#include <iostream>

auto parallelPow(int num)
{
  double res = 1.0;

  #pragma omp parallel for reduction(* : res)
  for (int i = 0; i < num; i++)
    res *= 2;

  return res;
}

int main()
{
  auto res = parallelPow(116);
  std::cout << res << std::endl;

  return 0;
}
